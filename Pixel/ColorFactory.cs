﻿namespace Pixel
{
    public class ColorFactory
    {
        public BaseColor GetColor(string code)
        {
            switch (code.ToUpper())
            {
                case ".": return new ColorDot();
                case "O": return new ColorO();
                case "P": return new ColorP();
                case "#": return new ColorHash();
                default: return null;
            }
        }

    }

    

    
}
