﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pixel
{
    public static class Reader
    {
        public static int ReadNumber(string msg)
        {
            try
            {
                Console.WriteLine(msg);
                string value = Console.ReadLine();
                return Int32.Parse(value);
            }
            catch (Exception)
            {
                Console.WriteLine("Número inválido. Digite novamente.");
                return ReadNumber(msg);
            }
        }

        public static BaseColor ReadColor(string msg, ColorFactory factory)
        {
            try
            {
                Console.WriteLine(msg);
                string value = Console.ReadLine();
                var color = factory.GetColor(value);
                if (color == null)
                    throw new Exception("Cor invalida");
                return color;
            }
            catch (Exception)
            {
                Console.WriteLine("Cor inválida. Digite novamente.");
                return ReadColor(msg, factory);
            }
        }

    }
}
