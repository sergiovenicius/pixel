﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pixel
{
    public class Matrix
    {
        private int lines;
        private int columns;
        private IColor baseColor;

        private List<List<IColor>> paper = new List<List<IColor>>();

        public Matrix(int lines, int cols, IColor backgroundColor)
        {
            this.lines = lines;
            this.columns = cols;
            this.baseColor = backgroundColor;

            Init();
            InitExampleP();
        }

        private void Init()
        {
            paper.Clear();
            
            for (int a = 0; a < lines; a++)
            {
                paper.Add(new List<IColor>());
                for (int b = 0; b < columns; b++)
                    paper.Last().Add(baseColor);
            }

        }

        private void InitExampleP()
        {
            paper[0][0] = new ColorDot();
            paper[1][0] = new ColorDot();
            paper[2][0] = new ColorDot();
            paper[3][0] = new ColorDot();

            paper[0][1] = new ColorHash();
            paper[1][1] = new ColorHash();
            paper[2][1] = new ColorHash();
            paper[3][1] = new ColorHash();

            paper[0][2] = new ColorHash();
            paper[1][2] = new ColorDot();
            paper[2][2] = new ColorHash();
            paper[3][2] = new ColorDot();

            paper[0][3] = new ColorHash();
            paper[1][3] = new ColorDot();
            paper[2][3] = new ColorHash();
            paper[3][3] = new ColorDot();

            paper[0][4] = new ColorDot();
            paper[1][4] = new ColorHash();
            paper[2][4] = new ColorDot();
            paper[3][4] = new ColorDot();

            paper[0][5] = new ColorDot();
            paper[1][5] = new ColorDot();
            paper[2][5] = new ColorDot();
            paper[3][5] = new ColorDot();
        }

        public void Repaint()
        {
            Console.WriteLine("");
            Console.WriteLine("");

            for (int line = 0; line < lines; line++)
            {
                Console.WriteLine("");
                for (int col = 0; col < columns; col++)
                    paper[line][col].Paint();
            }
            Console.WriteLine("");
            Console.WriteLine("");
        }

        private bool IsValid(IColor newColor, int line, int col)
        {
            if (line < 0 || col < 0) return false;
            if (line >= this.lines || col >= this.columns) return false;
            if (newColor == null) return false;

            return true;
        }

        public void SetColor(IColor newColor, int line, int col, Type referenceType)
        {
            if (!IsValid(newColor, line, col))
                return;

            if (referenceType == null)
                referenceType = paper[line][col].GetType();

            if (!IsTheSame(paper[line][col], referenceType))
                return;

            paper[line][col] = newColor;

            if (IsValid(newColor, line + 1, col))
                SetColor(newColor, line + 1, col, referenceType);

            if (IsValid(newColor, line, col + 1))
                SetColor(newColor, line, col + 1, referenceType);

            if (IsValid(newColor, line - 1, col))
                SetColor(newColor, line - 1, col, referenceType);

            if (IsValid(newColor, line, col - 1))
                SetColor(newColor, line, col - 1, referenceType);
        }

        private bool IsTheSame(IColor item, Type sameType)
        {
            if (item == null) return false;
            return item.GetType() == sameType;
        }

    }

    

    
}
