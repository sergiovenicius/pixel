﻿// See https://aka.ms/new-console-template for more information

using Pixel;


Matrix matrix = new Matrix(4, 6, new ColorDot());
ColorFactory factory = new ColorFactory();

do
{
    Console.Clear();

    matrix.Repaint();

    int linha = Reader.ReadNumber("Informe a linha do pixel que deseja pintar (iniciando da linha 0):");
    int coluna = Reader.ReadNumber("Informe a coluna do pixel que deseja pintar (iniciando da coluna 0):");
    BaseColor baseColor = Reader.ReadColor("Informe a cor que deseja usar (#, ., P ou O):", factory);

    matrix.SetColor(baseColor, linha, coluna, null);

} while (true);
